var fun={};
var ТАБЛИЦА='halls';

fun['ajax']=function(id,p){
$.post({url:'./php/ajax.php',data:{id:id,p:p},success:function (data){
	res=data;
	//console.log('bo52.data='+data);
},async:false});
return res;
};

function fun_prev(teg,poster){
	while (teg.prev().length>0){
		teg=teg.prev();
		if (teg[0].tagName=='TABLE'){
			if (teg.find(':contains("'+poster+'")').length>0) return teg;
		}
	}
	return [];
}

function fun_get_data(content){
var DATA=[];
var poster='Афиша Кинотеатра Салават';
//найти предыдущию таблицу содержащию правильную дату и афишу

var table_data,i_data;

$.each(content,function(index,e_hall){
//имя зала
i_hall=$(e_hall).text();

table_data=fun_prev($(e_hall).closest('table'),poster);

if (table_data.length==0) return;
//дата
i_data=table_data.find('strong').text().split(' ');
//i_months=months.indexOf(i_date[1]);
i_data=i_data[2]+' '+i_data[1]+' '+i_data[0];

if (!DATA[i_data]) DATA[i_data]={};

if (!DATA[i_data][i_hall]) DATA[i_data][i_hall]={};

$.each($(e_hall).closest('table').next().find('tr'),function(i_tr,e_tr){
	if (i_tr==0) return;
	DATA[i_data][i_hall][i_tr]=[];

    $.each($(e_tr).find('td'),function(i_td,e_td) {
		if (i_td==2) return;//возраст
        DATA[i_data][i_hall][i_tr].push($(e_td).text());
    });
});
});
return DATA;
}

function fun_rows(DATA){
var ROWS=[];
for (var i in DATA) {
	fun_halls(ROWS,DATA[i],i);
}
return ROWS;
}
function fun_halls(ROWS,halls,data){
for (var i in halls) {
	fun_hall(ROWS,halls[i],data,i);
}
}
function fun_hall(ROWS,row_hall,date,hall){
$.each(row_hall,function(index,row_film){
	ROWS.push([]);
	fun_film(ROWS,row_film,date,hall);
});
}
function fun_film(ROWS,film,date,hall){
$.each(film,function(index,e){
var val=e;
if (index==0)
    val=date+' '+e;
ROWS[ROWS.length - 1].push(val);
});
ROWS[ROWS.length - 1].push(hall);
}

function load_table(res){
var table=$('.data').find('tbody');
var tr;
table.empty();
$.each(res,function(index,row){
tr=$('<tr></tr>').appendTo(table);
$.each(row,function(i_cell,cell_e){
	$('<td>'+cell_e+'</td>').appendTo(tr);
});
});
}

function event_filter_click(e){
    filter();
}

function filter(){
    var i=0;
    var months = ["января", "февраля", "марта", "апреля", "мая", "июня","июля", "августа", "сентября", "октября", "ноября", "декабря"];
    var Film=$('#film')[0].value;
    var film=Film!=''?' film LIKE "%'+Film+'%"':'';
    if (film!='') i++;

    var date = new Date($('#date').val());
    var t=$('#date').val().split("-");
    var day = date.getDate();
    if (Number.isNaN(day))
        day='';
    else if (day<10) day='0'+day;

    var month = date.getMonth() + 1;
    if (Number.isNaN(month))
        month='';
    else
        month= months[month-1];

    var year = date.getFullYear();
    if (Number.isNaN(year)) year='';
    date=year+' '+month+' '+day;
    if (year+month+day!='') {
        date = ' date LIKE "%' + date + '%"';
        if (i>0) date = ' && '+date;
        i++;
    }
    else
        date='';

    var where=film+date;
    if (where=='')
        $('#show-filter').css('display','none');
    else {
        $('#show-filter').css('display', 'block');
        var text='Фильтр по ';
        if (date!='') text+='Дате="'+year+' '+month+' '+day+'" ';
        if (film!='') text+='Фильму="'+Film+'"';
        $('#show-filter').text(text);
    }
    fun_parsing(where);
}

function event_filter_clear_click(e){
    $('#show-filter').css('display','none');

    //var film=$('#film')[0].value;
    //if (film.value!='') film.value='';

    //var date=$('#date')[0];
    //if (date.value!='') date.value='';

    var res=fun['ajax']('parsing.where',[ТАБЛИЦА]);
    eval('res='+res+';');
    load_table(res);
}

function event_filter_update_click(e){
    fun_parsing();
}

function fun_parsing(where){
var res;
if (!where){
    $('#show-filter').css('display','none');
    var test=fun['ajax']('parsing.update');
    $('.temp').html(test);

    var content=$('strong:contains("ЗАЛ")');
    var DATA=fun_get_data(content);


    var ROWS=fun_rows(DATA);
    res=fun['ajax']('parsing.save',[ТАБЛИЦА,ROWS]);
    $('.temp').empty();
} else
	res=fun['ajax']('parsing.where',[ТАБЛИЦА,where]);

eval('res='+res+';');
load_table(res);
}
