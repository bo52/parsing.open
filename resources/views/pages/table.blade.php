@section('table')
    <div class="temp">

    </div>
    <table  class="data table-fixed-head table table-striped table-bordered table-hover table-condensed table-sm">
        <thead class="thead-dark">

        @include('pages.filter')
        <tr class="d-flex flex-row justify-content-between">@yield('filter')</tr>
        <tr><th>Дата</th><th>Имя</th><th>Цена</th><th>Зал</th></tr>
        </thead>
        <tbody></tbody>
    </table>
@stop
