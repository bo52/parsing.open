@extends('index')
@include('pages.table')
@include('pages.filter')

@section('content')
    @yield('table')
    <script src="{{asset('update.js')}}"></script>
@stop
