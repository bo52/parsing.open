@section('filter')
<div class="container container-filter">
    <div id="_filter" class="container">

        <div class="form-group has-success">
            <label for="date" class="col-sm-1 control-label">Дата</label>
            <div class="col-sm-3">
                <input id="date" type="date" class="form-control input-xs" placeholder="Введите дату">
            </div>

        <label for="film" class="col-sm-1 control-label">Фильм</label>
        <div class="col-sm-3">
        <input id="film" type=""text" class="form-control input-xs" placeholder="Введите название фильма">
        </div>

            <button for="_filter" class="_btn-execute btn btn-primary" onclick="event_filter_click($(this))">Выполнить</button>
            <button class="btn btn-light" onclick="event_filter_clear_click($(this))">Сбросить</button>
            <button class="btn btn-light" onclick="event_filter_update_click($(this))">Обновить</button>
        </div>

    </div>
    <label style="display:none" id="show-filter" class="control-label">Фильтр по Дате="" и Фильму=""</label>
</div>
@stop
