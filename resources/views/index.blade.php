<!DOCTYPE HTML>
<html lang=""ru-RU">
<head>
    <meta charset="UTF-8">
    <title>{{$title}}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>

    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/table-fixed.css')}}"/>
    <script src="{{asset('bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('script.js')}}"></script>
</head>
<body class="parsing">
<div class="container">
    @yield('content')
</div>
</body>
</html>
