<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class sitecontroller extends Controller
{
    function Poster(){
        return 'Афиша Кинотеатра Салават';
    }

    public function index(){
        $data=['title'=>$this->Poster()];
        return view('pages.get',$data);
    }

    public function update(){
        $data=['title'=>$this->Poster()];
        return view('pages.update',$data);
    }
}
